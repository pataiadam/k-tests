import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isSideBar: true
  },
  mutations: {
    changeIsSideBar (state) {
      state.isSideBar = !state.isSideBar
    }
  },
  actions: {
    switchSidebar ({ commit }) {
      commit('changeIsSideBar')
    }
  },
  modules: {
  }
})
