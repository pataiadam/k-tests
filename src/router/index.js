import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

import About from '../views/about/Main.vue'
import PageOne from '../views/about/PageOne.vue'
import PageTwo from '../views/about/PageTwo.vue'

import Contact from '../views/Contact.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: About,
    children: [{
      path: '/about/page-one',
      component: PageOne
    }, {
      path: '/about/page-two',
      component: PageTwo
    }]
  },
  {
    path: '/contact',
    name: 'Contact',
    component: Contact
  },
  {
    path: '/login',
    name: 'LoginRedirect',
    beforeEnter: (to, from, next) => {
      const code = to.query.code
      if (code) {
        console.log(code)
        // await axios.post('/auth/login', { data: { code } })
        // ha sikerult: next('/')
        // ha nem sikerult: next('/login-failed')
      }
      next('/')
    }
  }
]

const router = new VueRouter({
  routes
})

export default router
