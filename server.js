const express = require('express')
const app = express()
const port = 3000

app.use(express.static('dist'))

app.get('/ms-auth-redirect', (req, res) => {
  console.log(req.query)
  console.log('... authentikaciok...')
  res.redirect('/')
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
